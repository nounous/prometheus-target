#!/usr/bin/env python3
import argparse
import json
import os

import ldap


path = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Generate target lists for prometheus")
	args = parser.parse_args()

	with open(os.path.join(path, 'prometheus-target.json')) as file:
		config = json.load(file)

	base = ldap.initialize(config['ldap']['server'])
	if config['ldap']['server'].startswith('ldaps://'):
		base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
		base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)

	machines = base.search_s('ou=hosts,dc=crans,dc=org', ldap.SCOPE_SUBTREE, '(&(objectClass=ipHost)(description=monitoring:*))')
	all_profiles = set()
	machines = {
		machine[1]['cn'][0].decode('utf-8'): {
			profile.decode('utf-8').removeprefix('monitoring:')
			for profile in machine[1]['description']
			if profile.decode('utf-8').startswith('monitoring:')
		} for machine in machines
	}

	profiles = {}
	for machine in machines:
		for profile in machines[machine]:
			profiles.setdefault(profile, []).append(machine)

	for profile, machines in profiles.items():
		machines = sorted(machines)
		content = [{"targets": machines}]
		with open(os.path.join(path, 'generated', f'{profile}.json'), 'w') as file:
			json.dump(content, file, indent=4)
